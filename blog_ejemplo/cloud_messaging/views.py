from django.shortcuts import render

from django.views.generic import View

# Create your views here.
class ServiceWorkerView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'cloud_messaging/firebase-messaging-sw.js', content_type="application/x-javascript")
