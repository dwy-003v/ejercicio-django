from django.contrib.auth.models import User
from rest_framework import serializers
from blog.models import Post
from gestion_usuarios.models import PefilUsuario
from django.urls import reverse


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=255, style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def update(self, instance, validated_data):
        user = User.objects.get(username=instance)
        user.username = validated_data["username"]
        user.email = validated_data["email"]
        user.set_password(validated_data["password"])
        user.save()
        return user

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

class PerfilUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = PefilUsuario
        fields = ['url','user', 'foto_perfil']


class PostSerializer(serializers.ModelSerializer):
    url_local = serializers.SerializerMethodField('get_product_name')
    class Meta:
        model = Post
        fields = ['id', 'url', 'autor', 'titulo',
                  'contenido', 'fecha_publicacion', 'imagen', 'url_local']
    
    def get_product_name(self, obj):
        url = reverse('blog:detalle', kwargs={'pk':obj.pk })
        #/detalle/pk
        return url   

