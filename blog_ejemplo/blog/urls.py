from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views


app_name = 'blog'

urlpatterns = [
    path('', views.PostListView.as_view(), name='index'),
    path('detalle/<int:pk>', views.PostDetailView.as_view(), name='detalle'),
    path('crear', views.post_crear, name='crear'),
]

